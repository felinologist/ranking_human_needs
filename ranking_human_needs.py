
# get all combos
def get_cartesian_product(data):
    cartesian_list = [] * (6 ** 2)
    for key,value in data.items():
        for subkey,subvalue in data.items():
            if key != subkey:
                cartesian_list.append([key,subkey])
    return cartesian_list

#remove duplicates like love,growth vs growth, love
def dedup_subarrays(cartesian_data):
    target_list = [] * len(cartesian_data)
    for ix in range(len(cartesian_data)):
        cartesian_data[ix] = sorted(cartesian_data[ix])
    for item in (sorted(cartesian_data)):
        if not item in target_list:
            target_list.append(item)
    return (target_list)

#ask iteratively about all combos
def interview(data,target_dict):
    for slot in data:
        attrib1 = slot[0]
        attrib2 = slot[1]
        print(f'{slot[0]} vs {slot[1]} Which one is more important to you? \n Hit 1 for {slot[0]} and 2 for {slot[1]}')
        response = input()
        assert (response == '1' or response == '2')
        if input == '1':
             target_dict[attrib1] = target_dict[attrib1] + 1
        else:
             target_dict[attrib2] = target_dict[attrib2] + 1
        pass

# defining print function
def get_results(data):  
    for key,value in data.items():
        print(f'{key}={value}')
    pass

def needs_processor():

    # listing human needs
    needs_dict = {}
    needs_dict['security'] = 0
    needs_dict['importance'] = 0
    needs_dict['variety'] = 0
    needs_dict['love'] = 0
    needs_dict['growth'] = 0
    needs_dict['contribution'] = 0


    combos = get_cartesian_product(needs_dict)
    interview_data = (dedup_subarrays(combos))
    interview(interview_data,needs_dict)
    get_results(needs_dict)

if __name__ == "__main__":
    needs_processor()